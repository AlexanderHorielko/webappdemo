import 'package:flutter/material.dart';
import 'package:web_app_example/config.dart';

import 'new_board_form.dart';

class AddCard extends StatelessWidget {
  const AddCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildNewBoardWidget(),
            SizedBox(width: 10),
            SizedBox(
              width: 40,
              height: 40,
              child: FloatingActionButton(
                child: Icon(Icons.add_rounded),
                backgroundColor: GlobalColors.purple,
                onPressed: () async {
                  await showDialog(
                    context: context,
                    builder: (context) {
                      return NewBoardForm();
                    },
                  );
                },
              ),
            ),
            SizedBox(width: 10),
            Opacity(
              opacity: 0,
              child: _buildNewBoardWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildNewBoardWidget() {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: GlobalColors.darkGrey,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Text(
        'New board'.toUpperCase(),
        style: TextStyle(
          color: Colors.white,
          fontSize: 7,
        ),
      ),
    );
  }
}

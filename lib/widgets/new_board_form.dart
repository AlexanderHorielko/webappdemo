import 'package:flutter/material.dart';
import 'package:web_app_example/bloc/boards_bloc.dart';
import 'package:web_app_example/config.dart';

class NewBoardForm extends StatefulWidget {
  NewBoardForm({Key key}) : super(key: key);

  @override
  _NewBoardFormState createState() => _NewBoardFormState();
}

class _NewBoardFormState extends State<NewBoardForm> {
  final _formKey = GlobalKey<FormState>();
  String titleValue = '';
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      content: IntrinsicHeight(
        child: Container(
          width: 355,
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Opacity(
                      opacity: 0,
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Icon(
                          Icons.close_rounded,
                        ),
                      ),
                    ),
                    Text(
                      'New board'.toUpperCase(),
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    InkWell(
                      borderRadius: BorderRadius.circular(100),
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Icon(
                          Icons.close_rounded,
                        ),
                      ),
                      onTap: () {
                        Navigator.maybeOf(context).pop();
                      },
                    )
                  ],
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(110)),
                    ),
                    hintText: 'Example Board',
                    contentPadding: EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 22,
                    ),
                  ),
                  style: TextStyle(
                    fontSize: 12,
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter title!';
                    }
                    titleValue = value;
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(
                        GlobalColors.purple,
                      ),
                      padding: MaterialStateProperty.all<EdgeInsets>(
                        EdgeInsets.symmetric(
                          horizontal: 40,
                          vertical: 8,
                        ),
                      ),
                      shape: MaterialStateProperty.all<OutlinedBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    ),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();
                        BoardsBloc().changeBoards(titleValue);
                        Navigator.maybeOf(context).pop();
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            backgroundColor: Colors.green,
                            content: Text('Successfully added!'),
                          ),
                        );
                      }
                    },
                    child: Text(
                      'Create'.toUpperCase(),
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '../config.dart';

class Board extends StatelessWidget {
  final String title;
  const Board({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Center(
        child: Text(
          title,
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w700,
            color: GlobalColors.darkGrey,
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:web_app_example/config.dart';

class Header extends StatelessWidget {
  const Header({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Dashboard'.toUpperCase(),
            style: TextStyle(
                color: GlobalColors.purple,
                fontSize: 16,
                fontWeight: FontWeight.w700),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'Lorem ipsum',
            style: TextStyle(
              fontSize: 11,
              color: GlobalColors.darkGrey,
            ),
          ),
          Text(
            'At vero eos et accusam et justo duo',
            style: TextStyle(
              fontSize: 11,
              color: GlobalColors.darkGrey,
            ),
          ),
        ],
      ),
    );
  }
}

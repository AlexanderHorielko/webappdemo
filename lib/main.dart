import 'package:flutter/material.dart';
import 'package:web_app_example/bloc/boards_bloc.dart';
import 'package:web_app_example/config.dart';
import 'package:web_app_example/widgets/board.dart';
import 'package:web_app_example/widgets/header.dart';

import 'widgets/add_card.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: GlobalColors.purple,
      ),
      body: Container(
        padding: EdgeInsets.only(left: 100, right: 50),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 34,
              ),
              Header(),
              SizedBox(
                height: 15,
              ),
              Card(
                elevation: 2,
                margin: EdgeInsets.zero,
                child: Container(
                  padding: EdgeInsets.only(
                    left: 12,
                    right: 12,
                    bottom: 12,
                    top: 36,
                  ),
                  width: double.infinity,
                  child: Align(
                      alignment: Alignment.bottomLeft,
                      child: StreamBuilder<List<String>>(
                        stream: BoardsBloc().getBoards(),
                        initialData: [],
                        builder: (context, AsyncSnapshot<List<String>> snapshot) {
                          final boards = snapshot.data.reversed.toList();
                          return Wrap(
                            spacing: 15,
                            runSpacing: 12,
                            children: [
                              for (var i = 0; i < boards.length; i++)
                                Board(
                                  title: boards[i],
                                ),
                              AddCard(),
                            ],
                          );
                        },
                      )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

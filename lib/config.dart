import 'package:flutter/material.dart';

class GlobalColors {
  static const Color purple = Color.fromRGBO(95, 58, 255, 1);
  static const Color darkGrey = Color.fromRGBO(46, 46, 46, 1);
}

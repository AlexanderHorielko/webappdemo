import 'dart:async';

class BoardsBloc {
  static final BoardsBloc _singleton = BoardsBloc._internal();
  factory BoardsBloc() {
    return _singleton;
  }
  BoardsBloc._internal();

  List<String> _boards = [];

  StreamController<List<String>> _boardsController =
      StreamController<List<String>>.broadcast();

  void changeBoards(String value) {
    _boards.add(value);
    _boardsController.add(_boards);
  }

  Stream<List<String>> getBoards() {
    return _boardsController.stream;
  }
}
